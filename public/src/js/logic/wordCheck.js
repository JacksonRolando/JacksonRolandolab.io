const fs = require('fs');

const wordFile = './assets/words/wordle-official.txt';

var words = fs.readFileSync(wordFile, 'UTF8', function (err, data) {
    if (err) throw err;
    return data;
});

module.exports = {
    isValidWord: (word) => {
        return words.includes(word.toLowerCase().trim());
    },
    // 2 - correct letter in correct spot, 1 - correct letter in wrong spot, 0 - wrong letter
    isLettersInWord: (word, correct) => {
        var letters = word.toLowerCase().trim().split('');
        var incl = [];
        for (var i = 0; i < letters.length; i++){
            incl.push(correct.includes(letters[i]) ? correct.split('')[i] == letters[i] ? 2 : 1 : 0);
        }
        return incl;
    }
}
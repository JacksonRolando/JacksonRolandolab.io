
//-------------------- GLOBAL CONSTANTS ------------------------\\

const PAGE_ELEMENTS = {}
const ROWS = 6;
const CELLS_PER_ROW = 5;

const KEYCODE_RANGE = [65, 90];

const BACKSPACE_KEY = "Backspace";
const ENTER_KEY = "Enter";

const LETTERS = [
    ["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P"],
    ["A", "S", "D", "F", "G", "H", "J", "K", "L"],
    ["Z", "X", "C", "V", "B", "N", "M"],
];

//-------------------- IMPORTS ------------------------\\

//const submitWordRequest = require("./../logic/submit.js");

//-------------------- GLOBAL VARIABLES ------------------------\\

//Used to keep track of current row and column for words.
    //NOTE: 0 0  is top left
let currentRow = 0;
let currentCol = 0;

let cells = [];     //Will become double array of cell elements

//-------------------- GLOBAL FUNCTIONS ------------------------\\

/**
 * This method will clear ALL the children elements from a given
 * parent element.
 *
 * @param element The parent element to remove children from
 */
const clearChildrenElements = (element) => {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

/**
 * Renders the Wordle grid onto the webpage. This will fill the game
 * board with the cells, based on the specified ROWS and CELLS_PER_ROW (columns)
 */
const renderGrid = () => {
    cells = [];     //Empty the cells 2D array

    //Loop through rows
    for (let i = 0; i < ROWS; i++) {
        const newRow = PAGE_ELEMENTS.gridRow.cloneNode();   //Create row element
        cells.push([]);     //Add new array to cell 2d array for row

        //Loop through columns, create cells for row
        for (let c = 0; c < CELLS_PER_ROW; c++) {
            const newCell = PAGE_ELEMENTS.gridCell.cloneNode(true);
            newCell.row = i;
            newCell.column = c;
            cells[i].push(newCell);
            newRow.appendChild(newCell);
        }

        //Add row to grid
        PAGE_ELEMENTS.grid.appendChild(newRow);
    }
}   //end function renderGrid


const renderKeyboard = () => {
    LETTERS.forEach((letterArray, row) => {

        //Render extra invisible key for spacing
        if (row == 1) {
            const spacerKey = PAGE_ELEMENTS.keyboardKey.cloneNode(true);
            spacerKey.querySelector(".keyboard-key-content").innerText = "";
            spacerKey.querySelector(".keyboard-key-content").cssText += "opacity: 0;";
            spacerKey.style.cssText += "width: 7px; opacity: 0";
            PAGE_ELEMENTS.keyboard.appendChild(spacerKey);
        }

        //Render enter key
        if (row == 2) {
            const enterKey = PAGE_ELEMENTS.keyboardKey.cloneNode(true);
            enterKey.querySelector(".keyboard-key-content").innerText = "ENTER";
            enterKey.style.cssText += "width: 77px;";
            enterKey.onclick = onEnterPress;
            PAGE_ELEMENTS.keyboard.appendChild(enterKey);
        }

        //Render regular keys
        letterArray.forEach((letter, letterIndex) => {
            const newKey = PAGE_ELEMENTS.keyboardKey.cloneNode(true);
            newKey.querySelector(".keyboard-key-content").innerText = letter;
            newKey.onclick = () => {
                onKeyPress(letter);
            }
            newKey.setAttribute("id", letter);
            PAGE_ELEMENTS.keyboard.appendChild(newKey);
        })

        //Render backspace key
        if (row == 2) {
            const backKey = PAGE_ELEMENTS.keyboardKey.cloneNode(true);
            backKey.querySelector(".keyboard-key-content").innerText = "BACKSPACE";
            backKey.style.cssText += "width: 77px;";
            backKey.onclick = onBackspacePress;
            PAGE_ELEMENTS.keyboard.appendChild(backKey);
        }
    });
}   //end function renderKeyboard


/**
 * Retrieves the cell HTML element for a given row column position.
 *
 * @param row Row of cell element (starting at 0, ending at ROWS - 1)
 * @param col Column of cell element (starting at 0, ending at CELLS_PER_ROW - 1)
 * @return {*} The HTML cell element
 */
const getCell = (row, col) => {
    return cells[row][col];
}

/**
 * Updates the grid with the new cell to select for typing in a letter.
 * It shows the highlight under the current cell and removes the highlight
 * from the previous. Also updates the globals of currentRow and currentCol
 *
 * @param newRow The new row to set the currentRow to
 * @param newCol The new column to set the currentCol to
 */
const updateCurrentCell = (newRow, newCol) => {

    //Remove old cell's highlight
    const cellElement = getCell(currentRow, currentCol);
    if (cellElement) {
        cellElement.classList.remove("currentCell");
    }

    //Update globals
    if (newRow != null) currentRow = newRow;
    if (newCol != null) currentCol = newCol;

    //Add highlight to new cell
    const newCellElement = getCell(currentRow, currentCol);
    if (newCellElement) {
        newCellElement.classList.add("currentCell");
    }
}   //end function updateCurrentCell

//---- INPUT

/**
 * Function that executes upon any keystroke from the website. Only
 * works for keystrokes that are letters!
 *
 * @param capitalLetterPressed The capitalized letter that was pressed
 */
const onKeyPress = (capitalLetterPressed) => {
    if (currentRow >= ROWS) return;         //If our row is in the grid
    if (currentCol >= CELLS_PER_ROW) return;    //If not at end of row

    //Add letter and update position
    const cellElement = getCell(currentRow, currentCol);
    cellElement.querySelector(".grid-cell-content").innerText = capitalLetterPressed;
    updateCurrentCell(null, currentCol + 1);

    const keyElement = document.getElementById(capitalLetterPressed);
    if (keyElement) {
        keyElement.classList.add("keyActive");
        keyElement.classList.add("keyHover");
        setTimeout(() => {
            keyElement.classList.remove("keyActive");
            keyElement.classList.remove("keyHover");
        }, 80);
    }


}   //end function onKeyPress

/**
 * Function that executes upon the backspace key being keystroked.
 */
const onBackspacePress = () => {
    if (currentRow >= ROWS) return;     //If our row is not the first
    if (currentCol <= 0) return;    //If our cell isnt the first in the row

    //Remove old letter and update to previous cell
    const prevCol = currentCol - 1;
    const cellElement = getCell(currentRow, prevCol);
    cellElement.querySelector(".grid-cell-content").innerText = "";
    updateCurrentCell(null, prevCol);
}   //end function onBackspacePress

/**
 * Function that executes upon the enter key being keystroke
 */
const onEnterPress = () => {
    if (currentRow >= ROWS) return;
    if (currentCol < CELLS_PER_ROW) return;

    //TODO: Check if it's a valid word
    if (currentRow + 1 < ROWS) {
        updateCurrentCell(currentRow + 1, 0);
    }
}

//-------------------- ONLOAD --------------------\\

/**
 * Upon webpage being loaded
 */
window.onload = () => {
    PAGE_ELEMENTS.grid = document.getElementById("grid");
    PAGE_ELEMENTS.keyboard = document.getElementById("keyboard");
    PAGE_ELEMENTS.gridRow = document.querySelector(".grid-row").cloneNode();
    PAGE_ELEMENTS.gridCell = document.querySelector(".grid-cell").cloneNode(true);
    PAGE_ELEMENTS.keyboardKey = document.querySelector(".keyboard-key").cloneNode(true);

    clearChildrenElements(PAGE_ELEMENTS.grid);
    clearChildrenElements(PAGE_ELEMENTS.keyboard);

    renderGrid();
    renderKeyboard();

    window.addEventListener('keydown', function(event) {
        if (event.key === BACKSPACE_KEY) {
            onBackspacePress();
        } else if (event.key === ENTER_KEY) {
            onEnterPress();
        } else if (event.keyCode >= KEYCODE_RANGE[0] && event.keyCode <= KEYCODE_RANGE[1]) {
            onKeyPress(event.key.toUpperCase());
        }
    });

    updateCurrentCell(0, 0);
}   //end function

/*
setInterval( () => {
    console.log(currentRow, currentCol);
}, 500)

 */